import React from 'react'
import { Search } from '@react-vant/icons'
import './navbar.module.scss'
import { useNavigate }  from "react-router-dom"
function MyNavBar() {
    let route = useNavigate()
    return (
        <div>
            <nav>
                <span>&nbsp;&nbsp;</span>
                <i>Yoho!Buy</i>
                <Search fontSize={20} color='#fff' onClick={()=>route('/search')} />
            </nav>
        </div>
    )
}

export default MyNavBar