import React from 'react'
import { Tabs } from 'react-vant'
import { Toast, Cell, Image } from 'react-vant'


function MainTab() {
  return (
    <div className='demo-tabs'>
      <Tabs active='a'>
        {['鞋类', '服饰', '配饰', '儿童专区'].map((item, index) => (
          <Tabs.TabPane name={item} key={item} title={item}>
            {item}
          </Tabs.TabPane>
        ))}
      </Tabs>
    </div>
  )
}

export default MainTab