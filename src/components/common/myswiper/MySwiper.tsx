import React from 'react'
import { Swiper } from 'react-vant';
import { items } from './demo/items';
import './demo/base.scss';
import { useNavigate } from "react-router-dom"

function MySwiper() {
  let route = useNavigate() 
  return (
    <div className="demo-swiper"  onClick={() => route('/sweperDetail')}>
      <Swiper autoplay={5000} >{items}</Swiper>
    </div>
  )
}

export default MySwiper