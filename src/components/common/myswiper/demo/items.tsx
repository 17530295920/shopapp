import React from 'react'
import { Swiper, Toast } from 'react-vant'

const imgurl = [
  'http://anta-cn-web.obs.myhwclouds.com/anta-cn-web/descente/2021/04/12/32ec18c7f25df29141aa35968696f5c5.jpg', 
  'http://anta-cn-web.obs.myhwclouds.com/anta-cn-web/descente/2021/03/11/2ce63f438ec5541d11c43f3f5b1f5d79.jpg', 
  'http://anta-cn-web.obs.myhwclouds.com/anta-cn-web/descente/2021/03/11/ae819043ef32027068ce411ea0d3c8e8.jpg', 
  'http://anta-cn-web.obs.myhwclouds.com/anta-cn-web/descente/2021/04/12/c9d98391ba161b7451ec0e291654c3a3.jpg'
]


export const items = imgurl.map((imgurl, index) => (
  <Swiper.Item key={imgurl}>
    <div
     
    >
      <img src={imgurl} alt="" />
      {index + 1}
    </div>
  </Swiper.Item>
))
