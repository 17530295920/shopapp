import { Navigate, useRoutes } from "react-router-dom";
import Home from '../pages/Home/index'
import Class from '../pages/class'
import Shopcar from '../pages/shopcar'
import User from '../pages/user'
import Login from "../pages/login"
import Order from "../pages/orders"
import SweperDetail from '../pages/sweperdetail'
import Search from "../pages/search/search";
function Router() {
    let router = [
        {
            path:'/',
            element:(<Navigate to="/home"></Navigate>)
        },
        {
            path:'/home',
            element:(<Home/>)
        },
        {
            path:'/class',
            element:(<Class/>)
        },
        {
            path:'/shopcar',
            element:(<Shopcar/>)
        },
        {
            path:'/user',
            element:(<User/>)
        },
        {
            path:'/login',
            element:(<Login/>)
        },
        {
            path:'/order',
            element:(<Order/>)
        },
        {
            path:'/sweperDetail',
            element:(<SweperDetail/>)
        },
        {
            path:'/search',
            element:(<Search/>)
        },
    ]
    return useRoutes(router)
}
export default Router