import React from 'react'
import './registe.scss'
import { Input, Cell, hooks, Button } from 'react-vant';
import { 
  ArrowLeft,
  Manager,
  Lock,
  Phone,
  Chat,
  SendGift,
  Replay 
} from '@react-vant/icons'
import { NavLink } from 'react-router-dom';


function Registe() {
  const [state, updateState] = hooks.useSetState({
    text: '',

    password: '',
  })
  return (
    <div className='registe'>
      <div className='top'>
        <div onClick={()=>history.back()}>
          <ArrowLeft />
        </div>
          <span> 注册 </span>
          <span></span>
      </div>
      <Cell>
        <Input
          prefix={<Manager fontSize={'4vw'} />}
          value={state.text}
          onChange={text => updateState({ text })}
          placeholder='请输入账号'
        />
      </Cell>
      <Cell>
        <Input
          prefix={<SendGift fontSize={'4vw'} />}
          value={state.text}
          onChange={text => updateState({ text })}
          placeholder='请输入邮箱'
        />
      </Cell>
      <Cell>
        <Input
          prefix={<Lock fontSize={'4vw'} />}
          type='password'
          value={state.password}
          onChange={password => updateState({ password })}
          placeholder='请输入密码'
        />
      </Cell>
      <Cell>
        <Input
          prefix={<Phone fontSize={'4vw'} />}
          type='tel'
          suffix={<Button size="small" round color='#000' type="primary">获取验证码</Button>}
          placeholder="请输入手机号获取验证码"
        />
      </Cell>
      <Cell>
        <Input
          prefix={<Chat fontSize={'4vw'} />}
          type='number'
          value={state.text}
          onChange={text => updateState({ text })}
          placeholder='请输入验证码'
        />
      </Cell>
      <div className='verify '>
        <span>请将下列图片点击翻转至正向朝上</span>
        <span className='change'>换一批<Replay /></span>
      </div>
      <ul>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
      </ul>

      <Button 
        color='#ccc'
        type='primary' 
        size='large'
      >注册</Button>

      <p className='agree'>注册即表示您已阅读并同意</p>

      <p className='clause'>
        <NavLink to={'#'}>有货用户服务协议</NavLink>
        <NavLink to={'#'}>有货隐私条款</NavLink>
      </p>

    </div>
  )
}

export default Registe