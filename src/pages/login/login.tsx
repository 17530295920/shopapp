import React from 'react'
import './login.scss'
import { NavLink } from 'react-router-dom'
import { Cell, Input, hooks, Button } from 'react-vant'
import { ArrowLeft,Manager,Lock,Replay } from '@react-vant/icons'
import { useState,useEffect} from 'react'

function Login() {

  const [state, updateState] = hooks.useSetState({
    text: '',
    password: '',
  })
  const shoesDirection  = ['north', 'east', 'south', 'west']
  let [num,setNum] = useState(Math.floor(Math.random()*4))

  // console.log(num);
  function chageDirection(e:any){
    setNum(num+1)
    if(num == 4){
      setNum(0)
    }
    console.log(num);
    
    //---------------
    // console.log(e.target.tagName);
    // if(e.target.tagName=='IMG'){
    //   e.target.style.transform=`rotate(${num*90}deg)`
    // }
  }
  return (
    <div className='login'>
        <div className='topImg'>
          
          <img src="http://img11.static.yhbimg.com/yhb-img01/2018/03/26/10/01cf2c685c5d7ddbb21b7c7b961da77454.jpg?imageView2/2/w/750/h/290" alt="" />
          
          <div className='top'>
            <div onClick={()=>history.back()}>
              <ArrowLeft color='#fff' />
            </div>
            
            <NavLink to={'/registe'}> 注册</NavLink>
          </div>
        </div>
        <div className='login_info'>
          <Cell>
            <Input
              prefix={<Manager fontSize={'4vw'} />}
              value={state.text}
              onChange={text => updateState({ text })}
              placeholder='请输入账号'
            />
          </Cell>
          <Cell>
            <Input
              prefix={<Lock  fontSize={'4vw'}/>}
              value={state.password}
              type='password'
              onChange={password => updateState({ password })}
              placeholder='请输入密码'
            />
          </Cell>

          <div className='verify '>
            <span>请将下列图片点击翻转至正向朝上</span>
            <span className='change'>换一批<Replay /></span>
          </div>
          <ul onClick={(e)=>chageDirection(e)}>
            <li className={shoesDirection[num]}><img src="https://img.fishfay.com/shopgoods/1/112035523/zt-112035523/05451a1075959592633548afaa97a566.jpg" alt="" /></li>
            <li className={shoesDirection[num]}><img src="https://img.fishfay.com/shopgoods/1/122025520/zt-122025520/ad845bbae2c29ebeab75a87efa070a85.jpg" alt="" /></li>
            <li className={shoesDirection[num]}><img src="https://img.fishfay.com/shopgoods/4/122118041/zt-122118041/2e9cc3fe8e5246440548bbc5cba4115b.jpg" alt="" /></li>
            <li className={shoesDirection[num]}><img src="https://img.fishfay.com/shopgoods/4/112035528A/zt-112035528A.jpg" alt="" /></li>
          </ul>

          <Button 
            color='#ccc'
            type='primary'
            size='large'
          >登录</Button>

          <NavLink className='repassword' to={'/revise_password'}>忘记密码?</NavLink>

        </div>
          
    </div>
  )
}

export default Login