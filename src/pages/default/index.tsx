import React from "react"
import { NavLink } from "react-router-dom"
import Router from "../../router"
import style from './inde.module.css'
import { WapHomeO, AppsO, CartO,ManagerO } from '@react-vant/icons';
const Default = () => {
    return (
        <div className={style.box}>
            <footer>
                <NavLink to='/home'>
                    <div >
                        <WapHomeO fontSize={'6vw'} />
                    </div>
                    <div>主页</div>
                </NavLink>
                <NavLink to='/class'>
                    <AppsO fontSize={'6vw'}/>
                    <div>
                        分类
                    </div>
                </NavLink>
                <NavLink to='/shopcar'>
                    <CartO fontSize={'6vw'}/>
                    <div>
                        购物车
                    </div>
                </NavLink>
                <NavLink to='/user'>
                    <ManagerO fontSize={'6vw'}/>
                    <div>用户</div>
                </NavLink>
            </footer>
            <div>
                <Router></Router>
            </div>
        </div>
    )
}
export default Default