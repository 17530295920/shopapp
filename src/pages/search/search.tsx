import style from './inde.module.scss'
import { ArrowLeft, WapNav, WapHome, Cluster, Cart, Manager, AppsO, Search } from '@react-vant/icons'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { Input, Cell } from 'react-vant';
function Searchpage() {
    let [showwap, setShowwap] = useState(false)
    let [value, setValue] = useState('')
    let route = useNavigate()
    return (
        <div>
            <div className={style.nav}>
                <ArrowLeft onClick={() => history.back()} style={{ cursor: 'pointer' }} />
                <p>搜索</p>
                <AppsO className={style.waphove} onClick={() => setShowwap(!showwap)} style={{ cursor: 'pointer' }} />
                {
                    showwap ? (
                        <div className={style.wapbox} onClick={() => setShowwap(false)}>
                            <div className={style.active} onClick={() => route('/home')}>
                                <WapHome />
                                <span>首页</span>
                            </div>
                            <div className={style.active} onClick={() => route('/class')}>
                                <Cluster />
                                <span>分类</span>
                            </div>
                            <div className={style.active} onClick={() => route('/shopcar')}>
                                <Cart />
                                <span>购物车</span>
                            </div>
                            <div className={style.active} onClick={() => route('/user')}>
                                <Manager />
                                <span>我的</span>
                            </div>
                        </div>
                    ) : null
                }
            </div>
            <div style={{display:'flex'}}>
            <div className={style.sear}>
                <Search />
                <Input
                    placeholder="在这里搜索你想要的商品"
                    value={value}
                    onChange={setValue}
                    clearable
                    clearTrigger="always"
                    style={{ width: '80vw' }}
                />
            </div>
            <div>搜索</div>
            </div>
        </div>
    )
}
export default Searchpage