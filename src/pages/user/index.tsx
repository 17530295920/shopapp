import { useState, useEffect,useRef } from 'react'
import { parsePath, useNavigate } from 'react-router-dom';
import { ArrowLeft, WapNav, Arrow, EcardPay, SendGiftO, Logistics, CouponO, AfterSale, ServiceO, ChatO, WapHome, Cluster, Cart, Manager } from '@react-vant/icons';
import style from './inde.module.scss'
import activity1 from './img/活动1.jpg'
import activity2 from './img/活动2.jpg'
import { NavLink } from 'react-router-dom';
function User() {
  let wapbox = useRef(null)
  // useEffect(() => {
  //   console.log(document.getElementsByClassName('wapbox'));
  // }, [])
  const wapMove:any = () => {
    console.log(wapbox.current);
  }
  return (
    <div style={{ background: '#f0f0f0', paddingBottom: '14vw' }}>
      <nav className={style.topNav}>
        <ArrowLeft />
        <p>个人中心</p>
        <WapNav className={style.waphove} onClick={() => wapMove()} />
      </nav>

      <div className={style.loginBox}>
        <div className={[`${style.logButton}`, `${style.active}`].join(' ')}>
          <NavLink to={'/login'}>登录/注册 </NavLink>
        </div>

      </div>
      <div className={style.defaultchannel}>
        <div>默认频道</div>
        <div className={style.bu}>主页<Arrow color='#d2cdcc' /></div>
      </div>
      <div style={{ margin: '3vw 0' }}>
        <div className={style.defaultchannel} onClick={()=>route('/order')}>
          <div className={style.active}>我的订单</div>
          <div className={[`${style.bu}`, `${style.active}`].join(' ')}>全部订单<Arrow color='#d2cdcc' /></div>
        </div>
        <div className={style.order}>
          <div className={style.ord} style={{ 'borderTop': '1px solid #cecece' }}>
            <div onClick={()=>route('/order')}>
              <EcardPay fontSize='7vw' />
              <p>待付款</p>
            </div>
            <div onClick={()=>route('/order')}>
              <SendGiftO fontSize='7vw' />
              <p>待发货</p>
            </div>
            <div onClick={()=>route('/order')}>
              <Logistics fontSize='7vw' />
              <p>待收货</p>
            </div>
          </div>
          <div className={style.ord} style={{ 'borderTop': '1px solid #cecece' }}>
            <div onClick={()=>route('/order')}>
              {0}
              <p>待付款</p>
            </div>
            <div onClick={()=>route('/order')}>
              {0}
              <p>待发货</p>
            </div>
            <div onClick={()=>route('/order')}>
              {0}
              <p>待收货</p>
            </div>
          </div>
        </div>
      </div>

      <div className={style.discounts} style={{ marginBottom: '2vw' }}>
        {/* 优惠券 */}
        <div className={style.line}>
          <div style={{ display: 'flex', justifyContent: 'start' }}>
            <CouponO fontSize={"10vw"} /><span>优惠券</span>
          </div>
          <div>
            <Arrow color='#d2cdcc' />
          </div>
        </div>
        <div className={style.line}>
          <div style={{ display: 'flex', justifyContent: 'start' }}>
            <AfterSale fontSize={"10vw"} /><span>有货币</span>
          </div>
          <div>
            <Arrow color='#d2cdcc' />
          </div>
        </div>
      </div>


      <div className={style.discounts} style={{ marginBottom: '2vw' }}>
        {/* 消息 */}
        <div className={style.line}>
          <div style={{ display: 'flex', justifyContent: 'start' }}>
            <ChatO fontSize={"10vw"} /><span>消息</span>
          </div>
          <div>
            <Arrow color='#d2cdcc' />
          </div>
        </div>
        <div className={style.line}>
          <div style={{ display: 'flex', justifyContent: 'start' }}>
            <ServiceO fontSize={"10vw"} /><span>服务与支持</span>
          </div>
          <div>
            <Arrow color='#d2cdcc' />
          </div>
        </div>
      </div>


      <img src={activity1} alt="" style={{ display: 'block', width: '100%', height: '25vw', marginBottom: '4vw' }} />
      <img src={activity2} alt="" style={{ display: 'block', width: '100%', height: '30vw', marginBottom: '4vw' }} />

      <div className={style.showshop}>
        <div style={{ fontSize: '5vw', fontWeight: '600' }}>
          | 为你优选
        </div>
        <div>

        </div>
      </div>
    </div>
  )
}
export default User
