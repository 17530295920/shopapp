import React from 'react'
import './repassword.scss'
import { Cell, Input, hooks, Button } from 'react-vant'
import { Manager, Lock, Replay, ArrowLeft, } from '@react-vant/icons'


function Repassword() {
    const [state, updateState] = hooks.useSetState({
        text: '',
        password: '',
    })
    return (
        <div className='repassword'>
            <nav>
                <ArrowLeft color='#fff' onClick={()=>history.back()} />
                <span>修改密码</span>
                <span></span>
            </nav>
            
            <div className='password_info'>
                <Cell>
                    <Input
                    prefix={<Manager fontSize={'4vw'} />}
                    value={state.text}
                    onChange={text => updateState({ text })}
                    placeholder='请输入要修改密码得账号'
                    />
                </Cell>
                <Cell>
                    <Input
                    prefix={<Lock  fontSize={'4vw'}/>}
                    value={state.password}
                    type='password'
                    onChange={password => updateState({ password })}
                    placeholder='请输入修改后的密码'
                    />
                </Cell>

                <div className='verify '>
                    <span>请将下列图片点击翻转至正向朝上</span>
                    <span className='change'>换一批<Replay /></span>
                </div>
                
                <ul>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>

                <Button 
                    color='#ccc'
                    type='primary' 
                    size='large'
                >修改密码</Button>

            </div>

        </div>
    )
}

export default Repassword