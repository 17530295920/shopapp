import { useState,useEffect } from 'react'
import {postLogin} from '../../axios'
import MyNavBar from '../../components/content/navbar/MyNavBar'
import MySwiper from '../../components/common/myswiper/MySwiper'
import classStyle from './index.module.scss'
import MainTab from '../../components/content/mainTable/MainTab'


function App() {

  useEffect(()=>{
    postLogin().then((res: any) =>{
      console.log(res);
    })
  },[])

  return (
    <div className="App">
      <MyNavBar></MyNavBar>
      <MySwiper ></MySwiper>
      <div className={classStyle.classify}>
        <ul>
          <li><img src="http://img10.static.yhbimg.com/yhb-img01/2017/02/03/09/01ae835d5ae6d9502818daf351ad2db6cf.png?imageView2/2/w/98/h/98/q/60" alt="" />
            <span>新品到着</span>
          </li>
          <li><img src="http://img11.static.yhbimg.com/yhb-img01/2017/02/03/09/011004f5a04caaf9c18d7848049a75981e.png?imageView2/2/w/98/h/98/q/60" alt="" /> 
            <span>人气搭配</span>
          </li>
          <li><img src="http://img11.static.yhbimg.com/yhb-img01/2017/02/03/09/01d35157ab5942ea40b4f08a11c1680a17.png?imageView2/2/w/98/h/98/q/60" alt="" /> 
            <span>折扣专区</span>
          </li>
          <li><img src="http://img11.static.yhbimg.com/yhb-img01/2019/12/06/15/017563081b437d62c45c914a983354eb89.png?imageView2/2/w/98/h/98/q/60" alt="" /> 
            <span>有货拼团</span>
          </li>
          <li><img src="http://img10.static.yhbimg.com/yhb-img01/2017/02/03/09/01b097e06ac9fc78bbcc3d3e0dfbe01fcc.png?imageView2/2/w/98/h/98/q/60" alt="" /> 
            <span>全部分类</span>
          </li>
        </ul>
      </div>
      <MainTab></MainTab>
    </div>
  )
}

export default App
