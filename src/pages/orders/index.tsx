import { ArrowLeft, AppsO, BullhornO } from "@react-vant/icons"
import { useNavigate } from "react-router-dom"
import style from './index.module.scss'
import { Tabs } from 'react-vant'
import { useState } from "react"
import {WapHome,Cluster,Cart,Manager} from '@react-vant/icons'
function Order() {
    const items = ['全部', '待付款', '待发货', '待收货']
    const route = useNavigate()
    let [showwap ,setShowwap] = useState(false)
    return (
        <div style={{ background: '#f0f0f0' }}>
            <div className={style.nav}>
                <ArrowLeft onClick={()=>history.back()} style={{cursor:'pointer'}}/>
                <p>我的订单</p>
                <AppsO  onClick={()=>setShowwap(!showwap)} style={{cursor:'pointer'}}/>
                {
                    showwap ? (
                        <div className={style.wapbox}  onClick={() => setShowwap(false)}>
                            <div className={style.active} onClick={() => route('/home')}>
                                <WapHome />
                                <span>首页</span>
                            </div>
                            <div className={style.active} onClick={() => route('/class')}>
                                <Cluster />
                                <span>分类</span>
                            </div>
                            <div className={style.active} onClick={() => route('/shopcar')}>
                                <Cart />
                                <span>购物车</span>
                            </div>
                            <div className={style.active} onClick={() => route('/user')}>
                                <Manager />
                                <span>我的</span>
                            </div>
                        </div>
                    ) : null
                    }
            </div>
            <div className={style.scroll}>
                <BullhornO />
                <marquee behavior="scroll" scrollamount="1">关于防诈骗的重要提醒：我们不会以任何理由要求您退货退款，请您提高警惕。</marquee>
            </div>
            <div>
                <Tabs defaultActive={0} color="#333">
                    {items.map(item => (
                        <Tabs.TabPane key={item} title={item} >
                            <div style={true ? { background: "url('https://img01.yzcdn.cn/vant/empty-image-default.png') center 0vw no-repeat", backgroundSize: '40vw', height: '76vh' } : { background: '' }}>
                                <div className={style.suibian}>
                                    <p>您还没有订单</p>
                                    <div onClick={() => route("/home")}>随便逛逛</div>
                                </div>
                            </div>

                        </Tabs.TabPane>
                    ))}
                </Tabs>
            </div>
        </div>
    )
}
export default Order