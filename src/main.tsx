import React from 'react'
import ReactDOM from 'react-dom/client'
import Default from './pages/default/index'
import Home from './pages/Home/index'
import './index.css'
import { BrowserRouter } from 'react-router-dom'

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <BrowserRouter>
    <Default />
  </BrowserRouter>
)
